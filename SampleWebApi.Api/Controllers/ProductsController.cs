﻿using Microsoft.AspNetCore.Mvc;
using SampleWebApi.Application.Products.Services;

namespace SampleWebApi.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductsController : ControllerBase
    {
        [HttpGet]
        public IActionResult Get()
        {
            var productService = new ProductsService();
            productService.GetAll();

            return Ok();
        }
    }
}