﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Logging;
using SampleWebApi.Application.Exceptions;

namespace SampleWebApi.Api.Filters
{
    public class ExceptionsCatchFilter : ExceptionFilterAttribute
    {
        private readonly ILogger<ExceptionsCatchFilter> _logger;

        public ExceptionsCatchFilter(ILogger<ExceptionsCatchFilter> logger)
        {
            _logger = logger;
        }

        public override void OnException(ExceptionContext context)
        {
            var exception = context.Exception;
            _logger.LogError(exception.ToString());

            var exceptionType = exception.GetType();

            if (exceptionType == typeof(ValidationException))
            {
                context.Result = new BadRequestObjectResult((exception as ValidationException).Failures);
            }
            else if (exceptionType == typeof(WrongDataException))
            {
                context.Result = new BadRequestObjectResult(((WrongDataException)exception).Error);
            }
            else
            {
                context.Result = new BadRequestObjectResult(exception.Message);
            }
        }
    }
}
