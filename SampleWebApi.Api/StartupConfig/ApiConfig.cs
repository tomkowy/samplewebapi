﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using SampleWebApi.Api.Filters;

namespace SampleWebApi.Api.StartupConfig
{
    public static class ApiConfig
    {
        public static IServiceCollection AddApiSettings(this IServiceCollection services)
        {
            services.AddMvc(x =>
            {
                x.Filters.Add(typeof(ExceptionsCatchFilter));
            })
            .SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            return services;
        }
    }
}
