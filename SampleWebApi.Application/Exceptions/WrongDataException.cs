﻿using System;

namespace SampleWebApi.Application.Exceptions
{
    public class WrongDataException : Exception
    {
        public Error Error { get; set; }

        public WrongDataException(Error error) : base(error.Message)
        {
            Error = error;
        }
    }
}
