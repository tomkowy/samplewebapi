﻿using FluentValidation.Results;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SampleWebApi.Application.Exceptions
{
    public class ValidationException : Exception
    {
        public Dictionary<string, string[]> Failures;
        public ValidationException() : base("One or more validation faliures have occured")
        {
            Failures = new Dictionary<string, string[]>();
        }

        public ValidationException(List<ValidationFailure> failures) : this()
        {
            var propertyNames = failures
                .Select(x => x.PropertyName)
                .Distinct();

            foreach (var propertyName in propertyNames)
            {
                var propertyFailures = failures
                    .Where(x => x.PropertyName == propertyName)
                    .Select(x => x.ErrorMessage)
                    .ToArray();

                Failures.Add(propertyName, propertyFailures);
            }
        }
    }
}
