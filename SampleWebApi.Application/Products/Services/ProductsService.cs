﻿using SampleWebApi.Application.Exceptions;

namespace SampleWebApi.Application.Products.Services
{
    public class ProductsService : IProductsService
    {
        public void GetAll()
        {
            throw new WrongDataException(new Error("kod", "testowy tekst"));
        }
    }
}
