﻿namespace SampleWebApi.Application.Products.Services
{
    public interface IProductsService
    {
        void GetAll();
    }
}
